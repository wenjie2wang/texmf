#!/bin/bash

# set archive path
pkgDir=archive

# set file for package url
pkgUrl=PKG_URL

# create directory for source package
mkdir -p $pkgDir

# read package URL from PKG_URL and download it to $pkgDir
if [[ -e $pkgUrl ]]
then
    grep -E "^http|^ftp" $pkgUrl |
        while read oneLine; do
            echo "Try downloading from $oneLine"
            wget -N -P $pkgDir $oneLine
        done
else
    touch $pkgUrl
fi
