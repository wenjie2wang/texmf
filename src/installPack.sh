#!/bin/bash

# set archive path
pkgDir=archive

# create directory for source package
mkdir -p $pkgDir

# unzip all zip package in .tmpZip/ to ./tex/latex/
for pkg in $(ls $pkgDir/*.zip);
do
    # extract package name (with possible version number)
    pkgName="$(echo $pkg | sed "s/$pkgDir\///" | sed "s/[\.tds]*\.zip//")"
    # create directory named by package name
    mkdir -p tex/latex/$pkgName
    # unpack the source package
    unzip -o -qq $pkg -d tex/latex/$pkgName
    echo "$pkg unpacked to tex/latex/$pkgName"
    # add unpacked package to .gitignote if it is not there
    if ! grep -q $pkgName .gitignore
    then
        echo "tex/latex/$pkgName" >> .gitignore;
        echo "tex/latex/$pkgName added to .gitignore"
    fi
done;

# call texhash to refresh texmf
texhash .
