# texmf

Collection of LaTeX and BibTeX style files for personal usage.
All files can be found from [CTAN](http://www.ctan.org/).


## Quick Start

For LaTeX and BibTeX style files, we just need

* clone the repository to `$HOME/texmf`;
* run `texhash $HOME/texmf` in terminal.

```bash
$ git clone https://github.com/wenjie2wang/texmf.git $HOME/texmf
$ texhash $HOME/texmf
```

## More on Packages

A simple workflow is designed.  We may specify the packages we need in a pure
text file named *PKG_URL* and `make upgrade` under *texmf* to install or upgrade
them.
